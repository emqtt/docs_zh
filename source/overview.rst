
=============
eMQTT项目简介
=============

eMQTT是采用Erlang/OTP开发，基于MQTT协议的,发布订阅模式(Publish/Subscribe)模式消息服务器。

TODO: 发布订阅图。转移到index.rst???

-------------
eMQTT项目
-------------

完整的MQTT V3.1/V3.1.1 协议规范支持

保持简单架构，专注接入层与消息路由

Scalable, Scalable, Massively Scalable…

支持插件方式扩展认证与ACL，定制Push、移动IM、物联网等应用

MQTT, HTTP Publish, WebSocket, Stomp, SockJS，CoAP多协议接口


开发历史
-------

MQTT协议
-------

Erlang/OTP平台
--------------

C1000K测试说明
--------------

....

eMQTT其他开源库
--------------

eSockd：通用的Erlang TCP服务端框架

emqttc：Erlang MQTT客户端库

emqtt_benchmark：MQTT连接测试工具

CocoaMQTT：Swift语言MQTT客户端库

QMQTT：QT框架MQTT客户端库

